package com.volidar.elite.web.control

class EventAccumulatingControlHandler(maxValue: Int, action: (Double) => Unit) extends TimeAccumulatingControlHandler(action) {
  private var value = 0

  override def store(amount: Double): Unit = {
    if(amount > 0 && value < maxValue) {
      value += 1
    } else if (amount < 0 && value > 0) {
      value -= 1
    }
    super.store(1.0 * value * value / maxValue / maxValue)
  }
}
