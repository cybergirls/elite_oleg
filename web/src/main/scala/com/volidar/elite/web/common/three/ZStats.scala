package com.volidar.elite.web.common.three

import org.denigma.threejs.Stats

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("Stats")
class ZStats extends Stats {
  def update(): Unit = js.native
}
