package com.volidar.elite.web.model3d

import com.volidar.elite.web.common.LambertMaterial
import com.volidar.elite.web.common.three.ZMesh
import org.denigma.threejs._
import slogging.LazyLogging

import scala.scalajs.js

object AllianceFighter3d extends LazyLogging {
  def mesh = {
    val q = Math.PI / 2
    def toShape(points: (Double, Double)*): Shape = new Shape(js.Array(points.map{ case (x, y) => new Vector2(x, y)} :_* ))

    val screenMaterial = LambertMaterial(0xC0C0FF)
    val center = new ZMesh(new ShapeGeometry(toShape(-1.0 -> -1, 1.0 -> -1, 0.0 -> 1)), screenMaterial)

    val mainMaterial = LambertMaterial(0xE0E0E0)
    val rear = new ZMesh(new BoxGeometry(2, 2, 3), mainMaterial)
    center.add(rear.translateZ(-1.5))

    val cabin = new ZMesh(new BoxGeometry(2, 1.5, 1), mainMaterial)
    center.add(cabin.translateZ(0.5).translateY(-0.25))

    val screenRight = new ZMesh(new ShapeGeometry(toShape(-0.5 -> 0, 0.5 -> 0, -0.5 -> 0.5)), screenMaterial)
    val screenLeft = new ZMesh(new ShapeGeometry(toShape(-0.5 -> 0, 0.5 -> 0, 0.5 -> 0.5)), screenMaterial)
    val screenFront = new ZMesh(new PlaneGeometry(2, Math.sqrt(1.25)), screenMaterial)
    center.add(screenRight.translateX(-1).translateZ(0.5).translateY(0.5).rotateY(-q))
    center.add(screenLeft.translateX(1).translateZ(0.5).translateY(0.5).rotateY(q))
    center.add(screenFront.translateZ(0.5).translateY(0.75).rotateX(-1.107148718))

    val noseRight = new ZMesh(new ShapeGeometry(toShape(1.0 -> -1.0, 7.0 -> -0.25, 1.0 -> 0.5)), mainMaterial)
    val noseLeft = new ZMesh(new ShapeGeometry(toShape(-1.0 -> -1.0, -7.0 -> -0.25, -1.0 -> 0.5)), mainMaterial)
    val noseTop = new ZMesh(new PlaneGeometry(2, Math.sqrt(36.5625)), mainMaterial)
    val noseBottom = new ZMesh(new PlaneGeometry(2, Math.sqrt(36.5625)), mainMaterial)
    center.add(noseRight.translateX(-1).rotateY(-q))
    center.add(noseLeft.translateX(1).rotateY(q))
    center.add(noseTop.translateZ(4).translateY(0.125).rotateX(-1.446441332))
    center.add(noseBottom.translateZ(4).translateY(-0.625).rotateX(1.446441332))

    val motor = new ZMesh(new CylinderGeometry(0.5, 0.5, 3, 8), mainMaterial)
    val wing = new ZMesh(new BoxGeometry(5, 0.1, 3), mainMaterial)
    val cannon = new ZMesh(new CylinderGeometry(0.1, 0.1, 7, 6), LambertMaterial(0x404040))
    motor.add(wing.translateX(2.5).rotateX(-q))
    motor.add(cannon.translateX(5).translateY(2).translateZ(0).rotateX(0))
    center.add(motor.cloneMe().translateX(1.5).translateY(0.75).translateZ(-1.5).rotateX(q).rotateY(0.3))
    center.add(motor.cloneMe().translateX(1.5).translateY(-0.75).translateZ(-1.5).rotateX(q).rotateY(-0.3))
    center.add(motor.cloneMe().translateX(-1.5).translateY(0.75).translateZ(-1.5).rotateX(q).rotateY(Math.PI - 0.3))
    center.add(motor.cloneMe().translateX(-1.5).translateY(-0.75).translateZ(-1.5).rotateX(q).rotateY(Math.PI + 0.3))

    center.applyMatrix(new Matrix4().scale(new Vector3(1.5, 1.5, 1.5)))

    center
  }
}
