package com.volidar.elite.web.control

import org.denigma.threejs.PerspectiveCamera
import slogging.LazyLogging

class ControlledCamera(camera: PerspectiveCamera) extends ControlledObject with LazyLogging {
  private val maxXRotationSpeed = Math.PI / 1000
  private val maxZRotationSpeed = Math.PI / 1000
  private val maxZSpeed = 1.0

  override def rotateX(amount: Double): Unit = camera.rotateX(amount * maxXRotationSpeed)
  override def rotateZ(amount: Double): Unit = camera.rotateZ(amount * maxZRotationSpeed)
  override def move(amount: Double): Unit = camera.translateZ(-amount * maxZSpeed)
}
