package com.volidar.elite.web.common

import org.denigma.threejs.{Color, MeshLambertMaterial}

object LambertMaterial {
  def apply(color: Int) = {
    val material = new MeshLambertMaterial()
    material.color = new Color(color)
    material
  }
}
