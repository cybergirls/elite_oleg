package com.volidar.elite.web.communication

import com.volidar.elite.shared.protocol.{MessageFromServer, MessageToServer}
import com.volidar.elite.web.common.communication.websocket.WebSocketClient
import slogging.LazyLogging

class Communicator(onCommand: MessageFromServer => Unit) extends LazyLogging {
  private val webSocketClient = new EliteWebSocketClient()
  webSocketClient.connect()

  def send(command: MessageToServer): Unit = webSocketClient.send(command)

  private class EliteWebSocketClient extends WebSocketClient[MessageToServer, MessageFromServer] with Serialization {
    override def relativeSocketUrl = "/webSocket"
    override def onServerMessage(command: MessageFromServer): Unit = onCommand(command)
    //override def onConnect() = super.onConnect()
    //override def onClose(closedEvent: CloseEvent) = super.onClose(closedEvent) //TODO automatically reconnect
    //override def onError(event: ErrorEvent) = super.onError(event)
  }
}
