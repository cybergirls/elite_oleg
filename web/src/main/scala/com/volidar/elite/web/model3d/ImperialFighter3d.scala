package com.volidar.elite.web.model3d

import com.volidar.elite.web.common.LambertMaterial
import com.volidar.elite.web.common.three.{CanvasTexture, ZMesh}
import org.denigma.threejs.{CylinderGeometry, Matrix4, Mesh, MeshLambertMaterial, Shape, ShapeGeometry, SphereGeometry, Vector2}
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLCanvasElement
import slogging.LazyLogging

import scala.scalajs.js

object ImperialFighter3d extends LazyLogging {
  def mesh = {
    val cabinMaterial = LambertMaterial(0xE0C0C0)
    val cabin = new ZMesh(new SphereGeometry(2.0, 16, 16), cabinMaterial)

    val holderMaterial = LambertMaterial(0xC0C0C0)
    val holder = new Mesh(new CylinderGeometry(0.5, 0.5, 10.0, 8, 1, true), holderMaterial)
    holder.rotateX(Math.PI / 2)
    cabin.add(holder)

    val canvas = dom.document.createElement("canvas").asInstanceOf[HTMLCanvasElement]
    canvas.width = 128
    canvas.height = 128

    val context = canvas.getContext("2d")
    context.fillStyle = "#080808"
    context.fillRect(0, 0, 128, 128)

    context.strokeStyle = "#808080"
    context.beginPath()
    context.lineWidth = 2
    for(i <- 1 to 9) {
      val x = 128.0 / 10 * i
      context.moveTo(x, 128)
      context.lineTo(x, 17)
    }
    context.stroke()

    context.strokeStyle = "#FFFFFF"
    context.beginPath()
    context.lineWidth = 16
    context.moveTo(64, 128)
    context.lineTo(128, 17)
    context.lineTo(0, 17)
    context.lineTo(64, 128)
    context.stroke()

    val wingSegmentMaterial = new MeshLambertMaterial()
    wingSegmentMaterial.map = new CanvasTexture(canvas)

    val wingSegmentPoints: js.Array[Vector2] = js.Array((0.5, 0.0), (0.0, Math.sin(Math.PI / 3)), (1.0, Math.sin(Math.PI / 3))).map { case (x, y) => new Vector2(x, y) }
    val wingSegmentGeometry = new ShapeGeometry(new Shape(wingSegmentPoints))

    wingSegmentGeometry.applyMatrix(new Matrix4().makeTranslation(-0.5, 0, 0))
    wingSegmentGeometry.applyMatrix(new Matrix4().makeScale(5, 5, 5))

    var wingMesh = new ZMesh(wingSegmentGeometry, wingSegmentMaterial)
    (1 to 6).map(i => wingMesh.cloneMe().rotateZ(i * Math.PI / 3)).foreach(wingMesh.add _)
    wingMesh.add(wingMesh.cloneMe().rotateY(Math.PI))
    cabin.add(wingMesh.cloneMe().translateZ(-5))
    cabin.add(wingMesh.cloneMe().translateZ(5))

    cabin.applyMatrix(new Matrix4().makeRotationY(-Math.PI / 2))
    cabin
  }
}
