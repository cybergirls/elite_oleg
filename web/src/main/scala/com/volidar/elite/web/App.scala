package com.volidar.elite.web

import slogging.{ConsoleLoggerFactory, LogLevel, LoggerConfig}

import org.scalajs.dom
import org.scalajs.dom.raw.HTMLElement

object App {
  def main(args: Array[String]): Unit = {
    LoggerConfig.factory = ConsoleLoggerFactory()
    LoggerConfig.level = LogLevel.DEBUG
    val htmlElement = dom.document.getElementById("root").asInstanceOf[HTMLElement]
    val world = new World(htmlElement, dom.window.innerWidth, dom.window.innerHeight)
    world.render()
  }
}
