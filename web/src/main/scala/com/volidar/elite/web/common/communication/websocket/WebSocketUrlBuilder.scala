package com.volidar.elite.web.common.communication.websocket

import org.scalajs.dom._

/**
 * Helper object that constructs a web-socket URL from a relative-URL.
 *
 * Important in doing this is:
 * - that the resulting URL has to be absolute.
 * - because of this the port-number has to be included.
 * - the web-socket protocol (ws/wss) has to correspond
 *   to the http one (http/https).
 */
object WebSocketUrlBuilder {
  private def urlPrefix = s"$wsProtocol://$host$port"
  def fullUrl(relativeUrl: String): String = s"$urlPrefix$relativeUrl"

  private def host = window.location.hostname
  private def port = window.location.port match {
    case "" => ""
    case portStr => s":$portStr"
  }
  private def wsProtocol = window.location.protocol match {
    case https if https.startsWith("https") => "wss"
    case _ => "ws"
  }
}
