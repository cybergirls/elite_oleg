package com.volidar.elite.web

import com.volidar.elite.shared.protocol.{Created, MessageFromServer, Player, WorldUpdate}
import com.volidar.elite.web.common.three.ZStats
import com.volidar.elite.web.communication.Communicator
import com.volidar.elite.web.control.{ControlledCamera, ShipControls}
import com.volidar.elite.web.model3d.{AllianceFighter3d, ImperialFighter3d}
import org.denigma.threejs.extensions.Container3D
import org.denigma.threejs.extensions.controls.CameraControls
import org.denigma.threejs.{AmbientLight, Color, DirectionalLight, Object3D}
import org.scalajs.dom.raw.HTMLElement
import slogging.LazyLogging

class World(val container: HTMLElement, val width: Double, val height: Double) extends Container3D with LazyLogging {
  /* TODO use for window resize
    function onWindowResize() {
      camera.aspect = window.innerWidth / window.innerHeight;
      camera.updateProjectionMatrix();
      renderer.setSize( window.innerWidth, window.innerHeight );
    }
  */

  //val communicator = new Communicator(onServerMessage) //TODO DOIT reenable

  var meOpt = Option.empty[Player]
  var players = Map.empty[Int, (Player, Object3D)]

  def onServerMessage(message: MessageFromServer): Unit = message match {
    case Created(me) => meOpt = Some(me)
    case WorldUpdate(updatedPlayers) => ;//??? //TODO DOIT implement
  }

  override val controls: CameraControls = new ShipControls(container, new ControlledCamera(camera))



  def addToScene[T <: Object3D](obj: T)(modifier: T => Unit): T = {
    modifier(obj)
    scene.add(obj)
    obj
  }

  val fighter1 = addToScene(AllianceFighter3d.mesh)(_.position.set(10, 0, 0))
  val fighter2 = addToScene(ImperialFighter3d.mesh)(_.position.set(-10, 0, 0))

  scene.add(new AmbientLight(0x202020))

  val sunLight = addToScene(new DirectionalLight(0xFFFFFF, 1.0))(_.position.set(-1, 1, 1))

  camera.fov = 60
  camera.position.set(0, 60, 80)
  camera.lookAt(scene.position)

  renderer.setClearColor(new Color(0x000018))

  val stats = new ZStats()
  container.appendChild(stats.dom)

  override def onEnterFrame(): Unit = {
    super.onEnterFrame()
    stats.update()
    fighter1.rotateY(0.02)
    fighter2.rotateY(-0.02)
  }
}
