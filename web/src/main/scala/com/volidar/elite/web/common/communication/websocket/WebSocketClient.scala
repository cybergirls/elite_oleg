package com.volidar.elite.web.common.communication.websocket

import com.volidar.elite.web.common.communication.serialization.JsonSerialization
import org.scalajs.dom.raw._
import slogging.LazyLogging

import scala.util.{Failure, Success, Try}

abstract class WebSocketClient[TO_SERVER, FROM_SERVER] extends JsonSerialization[TO_SERVER, FROM_SERVER] with LazyLogging {
  private var optSocket: Option[WebSocket] = None
  private var earlyMessages = List.empty[TO_SERVER]

  def relativeSocketUrl: String

  protected def onServerMessage(incomingObj: FROM_SERVER): Unit
  protected def onDeserializationError(json: String, ex: Throwable): Unit = logger.error(s"Failed to de-serialize JSON: '$json' due to $ex", ex)
  protected def onConnect(): Unit = {
    logger.info("Connected to server via WebSocket")
    earlyMessages.foreach(send)
  }
  protected def onClose(closedEvent: CloseEvent): Unit = logger.info(s"WebSocket closed by the server: ${closedEvent.reason}")
  protected def onError(event: ErrorEvent): Unit = logger.error(s"WebSocket had an error: ${event.message}")

  final def send(data: TO_SERVER): Unit = optSocket match {
    case Some(activeSocket) => activeSocket.send(jsonWriter(data))
    case None => earlyMessages ::= data
  }
  final def connect(): Unit = {
    val socket = new WebSocket(WebSocketUrlBuilder.fullUrl(relativeSocketUrl))
    socket.onmessage = handleIncomingMessage(_: MessageEvent)

    socket.onopen = handleSocketConnected(socket, _: Event)
    socket.onclose = handleSocketClosed(_: CloseEvent)
    socket.onerror = handleSocketError(_: ErrorEvent)
  }

  private def handleIncomingMessage(event: MessageEvent): Unit = {
    val rawJsonStr = event.data.toString
    jsonReader(rawJsonStr) match {
      case Success(message) => onServerMessage(message)
      case Failure(ex) => onDeserializationError(rawJsonStr, ex)
    }
  }
  private def handleSocketConnected(connectedSocket: WebSocket, e: Event): Unit = {
    optSocket = Some(connectedSocket)
    Try(onConnect())
  }
  private def handleSocketClosed(event: CloseEvent) = {
    optSocket = None
    Try(onClose(event))
  }
  private def handleSocketError(event: ErrorEvent) = {
    optSocket = None
    Try(onError(event))
  }
}
