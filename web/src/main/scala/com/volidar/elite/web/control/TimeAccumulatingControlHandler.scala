package com.volidar.elite.web.control

class TimeAccumulatingControlHandler(action: (Double) => Unit) {
  case class State(lastAmount: Double, lastTime: Long, accumulatedAmount: Double)
  private var stateOpt = Option.empty[State]

  def store(amount: Double) = {
    val limitedAmount = Math.min(1, Math.max(amount, -1))
    val now = System.currentTimeMillis()

    stateOpt match {
      case None => stateOpt = Some(State(limitedAmount, now, 0))
      case Some(State(lastAmount, lastTime, accumulatedAmount)) =>
        stateOpt = Some(State(limitedAmount, now, accumulatedAmount + (limitedAmount + lastAmount) / 2 * (now - lastTime)))
    }
  }

  def update() = {
    stateOpt match {
      case None => action(0)
      case Some(State(lastAmount, lastTime, accumulatedAmount)) =>
        val now = System.currentTimeMillis()
        action(accumulatedAmount + lastAmount * (now - lastTime))
        stateOpt = Some(State(lastAmount, now, 0))
    }
  }
}
