package com.volidar.elite.web.communication

import com.volidar.elite.shared.protocol.{MessageFromServer, MessageToServer}
import com.volidar.elite.web.common.communication.serialization.JsonSerialization
import upickle.default._

import scala.util.Try

trait Serialization extends JsonSerialization[MessageToServer, MessageFromServer] {
  override protected val jsonWriter = (o: MessageToServer) => write(o)
  override protected val jsonReader = (s: String) => Try(read[MessageFromServer](s))
}
