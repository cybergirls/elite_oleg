package com.volidar.elite.web.common.three

import org.denigma.threejs.Texture
import org.scalajs.dom.raw.HTMLCanvasElement

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal("THREE.CanvasTexture")
class CanvasTexture extends Texture {
  def this(canvas: HTMLCanvasElement) = this()
}
