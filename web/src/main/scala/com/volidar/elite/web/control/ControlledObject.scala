package com.volidar.elite.web.control

trait ControlledObject {
  def rotateX(amount: Double): Unit
  def rotateZ(angle: Double): Unit
  def move(amount: Double): Unit
}
