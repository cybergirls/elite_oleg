package com.volidar.elite.web.common.three

import org.denigma.threejs.{Geometry, Material, Mesh}

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSGlobal, JSName}

@js.native
@JSGlobal("THREE.Mesh")
class ZMesh extends Mesh {
  def this(geometry: Geometry = js.native, material: Material = js.native) = this()

  @JSName("clone")
  def cloneMe(recursive: Boolean = true): ZMesh = js.native
}
