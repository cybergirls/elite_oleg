package com.volidar.elite.web.control

import org.denigma.threejs.extensions.controls.CameraControls
import org.scalajs.dom
import org.scalajs.dom.raw.{Element, Event, HTMLElement, KeyboardEvent}
import org.scalajs.dom.{MouseEvent, WheelEvent}
import slogging.LazyLogging

import scala.scalajs.js

class ShipControls(container: HTMLElement, controlled: ControlledObject) extends CameraControls with LazyLogging {
  //PointerLock: https://github.com/mrdoob/three.js/blob/master/examples/misc_controls_pointerlock.html
  val xRotationHandler = new TimeAccumulatingControlHandler(controlled.rotateX _)
  val zRotationHandler = new TimeAccumulatingControlHandler(controlled.rotateZ _)
  val movementHandler = new EventAccumulatingControlHandler(5, controlled.move _)

  override def onMouseDown(event: MouseEvent): Unit = {}
  override def onMouseMove(event: MouseEvent): Unit = {
    val baseHalfHeight = dom.window.innerHeight / 2
    val baseHalfWidth = dom.window.innerWidth / 2
    ((baseHalfHeight - event.clientY) / baseHalfHeight) match {
      case high if high > 0.1 => xRotationHandler.store(high - 0.01 / 0.99)
      case low if low < -0.1 => xRotationHandler.store(low + 0.01 / 0.99)
      case zero => xRotationHandler.store(0)
    }

    ((event.clientX - baseHalfWidth) / baseHalfWidth) match {
      case high if high > 0.1 => zRotationHandler.store(high - 0.01 / 0.99)
      case low if low < -0.1 => zRotationHandler.store(low + 0.01 / 0.99)
      case zero => zRotationHandler.store(0)
    }
  }
  override def onMouseUp(event: MouseEvent): Unit = {}
  override def onMouseWheel(event: MouseEvent): Unit = event match {
    case wheelEvent: WheelEvent => movementHandler.store(-wheelEvent.deltaY)
    case _ => logger.warn(s"wheel: ${event}")
  }
  def onKeyDown(event: KeyboardEvent): Unit = {
    event.keyCode match {
      case 38 => xRotationHandler.store(0.5)
      case 40 => xRotationHandler.store(-0.5)
      case 39 => zRotationHandler.store(1)
      case 37 => zRotationHandler.store(-1)
      case 87 => movementHandler.store(1) //w
      case 83 => movementHandler.store(-1) //s
      case _ => logger.debug(s"${event.keyCode}")
    }
  }
  def onKeyUp(event: KeyboardEvent): Unit = {
    event.keyCode match {
      case 38 => xRotationHandler.store(0)
      case 40 => xRotationHandler.store(0)
      case 37 => zRotationHandler.store(0)
      case 39 => zRotationHandler.store(0)
      case 83 => movementHandler.store(0) //s
      case 87 => movementHandler.store(0) //w
      case _ => logger.debug(s"${event.keyCode}")
    }
  }

  def onContextMenu(event: Event): js.Any = {event.preventDefault(); false}

  override def update(): Unit = {
    xRotationHandler.update()
    zRotationHandler.update()
    movementHandler.update()
  }
  override def enabled: Boolean = true

  def attach(el: Element) = {
    el.addEventListener("mousedown", (this.onMouseDown _).asInstanceOf[Function[Event, _]])
//    el.addEventListener("mousemove", (this.onMouseMove _).asInstanceOf[Function[Event, _]], false)
    el.addEventListener("mouseup", (this.onMouseUp _).asInstanceOf[Function[Event, _]], false)
    el.addEventListener("wheel", (this.onMouseWheel _).asInstanceOf[Function[Event, _]])
    el.addEventListener("contextmenu", (this.onContextMenu _).asInstanceOf[Function[Event, _]], false)
    dom.document.addEventListener("keydown", (this.onKeyDown _).asInstanceOf[Function[Event, _]], false)
    dom.document.addEventListener("keyup", (this.onKeyUp _).asInstanceOf[Function[Event, _]], false)
  }

  this.attach(container)
}
