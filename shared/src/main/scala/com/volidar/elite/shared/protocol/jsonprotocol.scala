package com.volidar.elite.shared.protocol

case class Vector(x: Double, y: Double, z: Double)
case class ObjectPosition(position: Vector, frontDirection: Vector, upDirection: Vector)

case class Player(id: Int, model: PlayerModel, position: ObjectPosition)

sealed trait MessageFromServer
case class Created(me: Player) extends MessageFromServer
case class WorldUpdate(players: List[Player]) extends MessageFromServer

sealed trait MessageToServer
case class UpdateMyPosition(position: ObjectPosition) extends MessageToServer

sealed trait PlayerModel
object PlayerModel {
  case object AllianceFighter extends PlayerModel
  case object ImperialFighter extends PlayerModel
}
