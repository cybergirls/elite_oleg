package com.volidar.elite.server.controller

import javax.inject.Inject

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import com.volidar.elite.server.actor.UniverseActor.{PlayerGone, PlayerJoined}
import com.volidar.elite.server.actor.UniverseHolder
import com.volidar.elite.shared.protocol.{MessageFromServer, MessageToServer}
import play.api.Environment
import play.api.libs.streams.ActorFlow
import play.api.mvc.{AbstractController, ControllerComponents, WebSocket}
import upickle.default.{read, write}

class MainController @Inject()(components: ControllerComponents, universe: UniverseHolder)(implicit actorSystem: ActorSystem, environment: Environment, materializer: Materializer)
  extends AbstractController(components) with LazyLogging {

  def index = Action {Ok(views.html.index())}

  def webSocket = WebSocket.accept[String, String] { request => ActorFlow.actorRef { client => Props(new CodecActor(client)) } }

  private def universeActor = universe.actorRef

  class CodecActor(client: ActorRef) extends Actor {
    universeActor ! PlayerJoined

    override def receive = {
      case in: String => universeActor ! read[MessageToServer](in)
      case out: MessageFromServer => client ! write(out)
    }
    override def postStop(): Unit = {
      super.postStop()
      universeActor ! PlayerGone
    }
  }
}
