package com.volidar.elite.server.actor

import javax.inject.Inject

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.typesafe.scalalogging.LazyLogging
import com.volidar.elite.server.actor.UniverseActor.{PlayerGone, PlayerJoined, SendUpdate}
import com.volidar.elite.shared.protocol._

import scala.util.Random

class UniverseHolder @Inject()(actorSystem: ActorSystem) {
  val actorRef = actorSystem.actorOf(Props(new UniverseActor()))
}

object UniverseActor {
  sealed trait UniverseMessage
  case object PlayerJoined extends UniverseMessage
  case object PlayerGone extends UniverseMessage
  case object SendUpdate extends UniverseMessage
}

class UniverseActor() extends Actor with LazyLogging {
  var state = Map.empty[ActorRef, Player]

  //TODO DOIT schedule periodic SendUpdate's

  def newPlayer = {
    val newId = if(state.isEmpty) 1 else state.map(_._2.id).max
    Player(newId, if(Random.nextBoolean) PlayerModel.AllianceFighter else PlayerModel.ImperialFighter,
      ObjectPosition(Vector(0.0, 0.0, 0.0), Vector(0.0, 0.0, 1.0), Vector(0.0, 1.0, 0.0)))
  }

  override def receive = {
    case PlayerJoined =>
      val created = newPlayer
      sender() ! Created(created)
      state += sender() -> created
    case PlayerGone => state -= sender()
    case UpdateMyPosition(position) => state.get(sender()) match {
      case None => logger.warn(s"Updated position of gone player.")
      case Some(player) => state += sender() -> player.copy(position = position)
    }
    case SendUpdate =>
      val update = WorldUpdate(state.values.toList)
      state.keys.foreach(_ ! update)
  }
}
