import sbt.Project.projectToRef

val Versions = new Object {
  val scala = "2.12.3"
  val bootstrap = "3.3.7" //DOIT find and remove not used
  val logback = "1.2.3"
  val uPickle = "0.4.4"
}

addCommandAlias("ep", "~exported-products")
addCommandAlias("ept", "~;exported-products;test:exported-products")
addCommandAlias("crep", ";clean;reload;exported-products")

name in ThisBuild := """elite"""
version in ThisBuild := "0.1-SNAPSHOT"

scalaVersion in ThisBuild := Versions.scala

resolvers in ThisBuild ++= Seq(
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
  sbt.Resolver.bintrayRepo("denigma", "denigma-releases"),
  Resolver.sonatypeRepo("snapshots")
)

lazy val server = (project in file("server"))
  .settings(
    libraryDependencies ++= Seq(
      guice,

      "com.vmunier" %% "scalajs-scripts" % "1.1.1",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
      "ch.qos.logback" % "logback-classic" % Versions.logback,

      "org.webjars" %% "webjars-play" % "2.6.2",
      "org.webjars" % "bootstrap" % Versions.bootstrap
    ),
    scalaJSProjects := jsProjects,
    pipelineStages in Assets := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    //triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    routesGenerator := InjectedRoutesGenerator
  )
  .dependsOn(sharedJVM)
  .aggregate(jsProjects.map(projectToRef): _*)
  .enablePlugins(PlayScala)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
  .settings(libraryDependencies ++= Seq(
//    "com.lihaoyi" %%% "autowire" % "0.2.5",
    "com.lihaoyi" %%% "upickle" % Versions.uPickle
//    "me.chrons" %%% "diode" % "1.0.0"
  ))

lazy val sharedJVM = shared.jvm
lazy val sharedJS = shared.js

lazy val web = (project in file("web"))
  .settings(
    scalaJSUseMainModuleInitializer := true,
    scalaJSUseMainModuleInitializer in Test := false,
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "0.9.3",
      "be.doeraene" %%% "scalajs-jquery" % "0.9.2",
      "biz.enef" %%% "slogging" % "0.5.2",
      "org.denigma" %%% "threejs-facade" % "0.0.77-0.1.8" //DOIT add "org.webjars" % "three.js" % "r77"
    ),
    jsDependencies ++= Seq(
      "org.webjars.bower" % "jquery" % "3.2.1" / "dist/jquery.js" minified "dist/jquery.min.js" commonJSName "jQuery",
      "org.webjars.bower" % "bootstrap" % Versions.bootstrap / "dist/js/bootstrap.js" minified "dist/js/bootstrap.min.js" commonJSName "bootstrap" dependsOn("dist/jquery.js")
    )
  )
  .dependsOn(sharedJS)
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)

lazy val jsProjects = Seq(web, sharedJS)

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value

scalacOptions in ThisBuild ++= Seq(
  "-Xcheckinit",                    //Wrap field accessors to throw an exception on uninitialized access.
  "-Xlog-reflective-calls",         //Print a message when a reflective method call is generated
  "-Xno-forwarders",                //Do not generate static forwarders in mirror classes.
  "-Xno-uescape",                   //Disable handling of \ u unicode escapes.
  "-Ybreak-cycles",                 //Attempt to break cycles encountered during typing
  "-Yinfer-argument-types",         //Infer types for arguments of overridden methods.
  "-Ypresentation-strict",          //Do not report type errors in sources with syntax errors.
  "-Ywarn-dead-code",               //Warn when dead code is identified.
  "-Ywarn-inaccessible",            //Warn about inaccessible types in method signatures.
  "-Ywarn-infer-any",               //Warn when a type argument is inferred to be `Any`.
  "-Ywarn-nullary-override",        //Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Ywarn-nullary-unit",            //Warn when nullary methods return Unit.
  "-Ypos-debug",                    //Trace position validation.

  "-deprecation",
  "-unchecked",
  "-feature"
)
